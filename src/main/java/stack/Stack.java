package stack;

/**
 * Created by arty on 23.05.2017.
 */
public class Stack {
    private int maxSize;
    private int top;
    private char[] stackArray;

    public Stack(int maxSize) {
        top = -1;
        this.maxSize = maxSize;
        stackArray = new char[maxSize];
    }

    public void push(char data) {
        if (!isFull()) {
            top++;
            stackArray[top] = data;
        }
    }

    public char pop() {
        char result = 0;
        if (!isEmpty()) {
            result = stackArray[top];
            top--;
        }
        return result;
    }

    public int peek() {
        if (!isEmpty()) {
            return stackArray[top];
        }
        return 0;
    }

    public boolean isEmpty() {
        return (top == -1);
    }

    public boolean isFull() {
        return (top == maxSize - 1);
    }
}
