package stack;

import java.util.Arrays;

/**
 * Created by arty on 23.05.2017.
 */
public class BracketChecker {
    String input;

    public BracketChecker(String input) {
        this.input = input;
    }

    public boolean check() {
        Stack stack = new Stack(input.length());
        for (int i = 0; i < input.length(); i++) {
            char ch = input.charAt(i);
            switch (ch) {
                case ('('):
                case ('{'):
                case ('['):
                    stack.push(ch);
                    break;
                case (')'):
                case ('}'):
                case (']'):
                    if (!stack.isEmpty()) {
                        char chx = stack.pop();
                        if (ch == '}' && chx != '{' ||
                                ch == ']' && chx != '[' ||
                                ch == ')' && chx != '(') {
                            System.out.println("Error at " + i);
                            return false;
                        }
                    } else {
                        System.out.println("Unexpected stack end");
                        return false;
                    }
                    break;
                default:
                    break;
            }
        }
        if (!stack.isEmpty()) {
            System.out.println("Error missing right delimiter");
            return false;
        }
        return true;
    }
}
