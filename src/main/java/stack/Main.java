package stack;

/**
 * Created by arty on 23.05.2017.
 */
public class Main {
    public static void main(String[] args) {
        Stack stack = new Stack(5);
        stack.push('a');
        stack.push('d');
        stack.push('a');
        stack.push('m');
        StringBuilder sb = new StringBuilder();
        while(!stack.isEmpty()){
            sb.append(stack.pop());
        }
        System.out.println(sb.toString());
    }
}
