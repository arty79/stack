package stack;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by arty on 23.05.2017.
 */
public class BracketApp {

    public static void main(String[] args) throws IOException {
        String input;
        while (true) {
            System.out.println("Input String");
            System.out.flush();
            input = getString();
            if (input.equals("")) {
                break;
            }
            BracketChecker checker = new BracketChecker(input);
            checker.check();
        }
    }

    public static String getString() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        return reader.readLine();
    }
}
