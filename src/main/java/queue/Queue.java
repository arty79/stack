package queue;

/**
 * Created by arty on 23.05.2017.
 */
public class Queue {
    private int maxSize;
    private int rear;
    private int front;
    private int count;
    private long[] queArray;

    public Queue(int maxSize) {
        this.maxSize = maxSize;
        rear = -1;
        front = 0;
        count = 0;
        queArray = new long[maxSize];
    }

    public void insert(long j) {
        if (rear == maxSize - 1) {
            rear = -1;
        }
        queArray[++rear] = j;
        count++;

    }

    public long remove() {
        long temp = queArray[front++];
        if (front == maxSize) {
            front = 0;
        }
        count--;
        return temp;
    }

    public long peekFront() {
        return queArray[front];
    }

    public boolean isEmpty() {
        return count == 0;
    }

    public boolean isFull() {
        return count == maxSize;
    }

}
