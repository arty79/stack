package sort;

/**
 * Created by arty on 24.05.2017.
 */
public class ArrayBubble {
    private long[] a;
    private int count;

    public ArrayBubble(int max) {
        a = new long[max];
        count = 0;
    }

    public void insert(long value) {
        a[count] = value;
        count++;
    }

    public void display() {
        for (int i = 0; i < count; i++) {
            System.out.println(a[i]);
        }
    }

    public void bubbleSort() {
        int out, in;
        for (out = count - 1; out > 1; out--) {
            for (in = 0; in < out; in++) {
                if (a[in] > a[in + 1])
                    swap(in, in + 1);
            }
        }
    }

    public void swap(int one, int two) {
        long temp = a[one];
        a[one] = a[two];
        a[two] = temp;
    }
}
