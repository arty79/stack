package selectsort;

/**
 * Created by arty on 24.05.2017.
 */
public class SelectSortApp {
    public static void main(String[] args) {
        int maxSize = 20;
        ArraySel arr = new ArraySel(maxSize);
        arr.insert(77);
        arr.insert(99);
        arr.insert(44);
        arr.insert(52);
        arr.insert(22);

        arr.insert(88);
        arr.insert(11);
        arr.insert(63);

        arr.display();
        arr.selectSort();
        arr.display();

    }
}
