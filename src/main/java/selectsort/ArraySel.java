package selectsort;

/**
 * Created by arty on 24.05.2017.
 */
public class ArraySel {
    private long[] arr;
    private int count;

    public ArraySel(int maxSize) {
        arr = new long[maxSize];
        count = 0;
    }

    public void insert(long l) {
        arr[count] = l;
        count++;
    }

    public void display() {
        for (int i = 0; i < count; i++) {
            System.out.println(arr[i]);
        }
        System.out.println();
    }

    public void selectSort() {
        int out, in, min;
        for (out = 0; out < count - 1; out++) {
            min = out;
            for (in = out + 1; in < count; in++) {
                if (arr[in] < arr[min]) {
                    min = in;
                }
            }
            swap(out, min);
        }
    }

    public void swap(int one, int two) {
        long temp = arr[one];
        arr[one] = arr[two];
        arr[two] = temp;
    }
}
